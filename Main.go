package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func init() {
	admin := GetAdmin()
	admin.GET("/ping", func(c *gin.Context) {
		var ping Ping
		if !insert(&ping) {
			c.JSON(500, map[string]string{"message": "database not connect"})
			return
		}
		c.JSON(200, ping)
	})
}

func main() {

	//Gin
	eng := GetEngine()
	if len(os.Args) < 3 {
		log.Fatal(eng.Run(GetIP() + ":" + GetPort()))
		return
	}

	log.Fatal(eng.Run(os.Args[1] + ":" + os.Args[2]))
}
