package main

import "github.com/jinzhu/gorm"

//Detailversion is
type Detailversion struct {
	gorm.Model
	ID        uint `gorm:"primary_key"`
	VersionID int
	From      string
	To        string
	Filesize  int64
}

func init() {
	db().AutoMigrate(&Detailversion{})
}
